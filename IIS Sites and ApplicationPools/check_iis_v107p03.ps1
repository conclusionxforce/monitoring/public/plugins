<#
  .SYNOPSIS
   script for nagios to check IIS Servers Sites and AppPool.
  .DESCRIPTION
   Auther Yossi Bitton yossi@edp.co.il
   Version 1.07patch03
   === begin block containing patch info
   Patch p03: 23.05.2022 by pieter.vanderwerf@ConclusionXforce.nl added support for Include and sites with spaces
   Patch p02: 16.06.2017 by jvandermeulen replaces "unknowns" with "unknown"
   Patch p01: 20.04.2017 by Nagios Enterprises to make it work with NCPA (Nagios Cross Platform Agent. Other known bugs: no timeout in script
   === end block containing patch info
   Date: 8-2016   
   Fixed: 20.12.2016
   1: Total sites ot application pools count fixed when -exclude not=$null.
   2: wrong result when using -Exclude. 
  .EXAMPLE
	.\check_iis.ps1 Sites -Exclude "site01,oldsite2"
	.\check_iis.ps1 Sites -Include "site01,site 02"
	.\check_iis.ps1 Sites -Include "site01,site 02" -DebugMode 1
	.\check_iis.ps1 AppPool 
	.\check_iis.ps1 Sites
	
	for Sites or AppPools with space in the name, use sitenames between "", devided by ,
	example: check_iis Sites -Include "site01,site with spaces"

	for Nagios NRPE edit NSC.ini or nsclient.ini and add the following line under section:
	[Wrapped Scripts] 
	 check_iis=check_iis.ps1 $ARG1$
	
	[Script Wrappings]
	ps1 = cmd /c echo scripts\%SCRIPT% %ARGS%; exit($lastexitcode) | powershell.exe -ExecutionPolicy Bypass -command -
	
	from nagios run:
	./check_nrpe -H [IIS IP Address] -t 30 -c check_iis -a '-CheckType Sites -Exclude site01,oldsite2' 
	or 
	./check_nrpe -H [IIS IP Address] -t 30 -c check_iis -a 'Sites site01,oldsite2' 

	working with NCPA-client:
	./check_ncpa.py -t [NCPA-API-TOKEN] -p 5693 -H [HOST] -M 'plugins/check_iis_v107p03.ps1' -q 'args=Sites'
	./check_ncpa.py -t [NCPA-API-TOKEN] -p 5693 -H [HOST] -M 'plugins/check_iis_v107p03.ps1' -q 'args=Sites,args="site 1,site2'
#>

[CmdletBinding()]
Param(
	[parameter(Mandatory = $true, Position = 1)] 
	[ValidateSet("Sites", "AppPool")] 
	[String]$CheckType ,
	[parameter(Mandatory = $false, Position = 2)]
	[int]$DebugMode , 
	[parameter(Mandatory = $false, Position = 3)]
	[String[]]$Exclude,
	[parameter(Mandatory = $false, Position = 4)]
	[String[]]$Include
)


Function Print_Debug ($msg) {
	if ($DebugMode) {
		Write-Host "$msg"
	}

}

#Load_Exchange_Module
function Load_IIS_Module() {
	Print_Debug "Load_IIS_Module..."
	$retCode = $false
	try {
		$desc = [System.Reflection.Assembly]::LoadFrom("C:\windows\system32\inetsrv\Microsoft.Web.Administration.dll" )
		Print_Debug "Load Status=$desc"
	}
	catch {
		$desc = $_.Exception.Message	
		Print_Debug $desc
	}
	if ($desc -ne $null) {
		$desc = "IIS module loaded successful"
		Print_Debug  $desc
		$retCode = $true
	}
	else {
		$desc = "failed to load IIS module"
		Print_Debug $desc
		$retCode = $false
	}
	return $retCode , $desc 
}

Function Get_AppPool_Status () {
	$retCode = $unknown
	$desc = $null
	$perfData = ""
	$failedAppPool = 0
	$descErr = @()
	$poolList = $Null
	try {
		$serverManager = [Microsoft.Web.Administration.ServerManager]::OpenRemote($server)
		if ($serverManager -ne $null) {
			Print_Debug "Connected to $serverManager"
			$allAppPool = $serverManager.ApplicationPools | Select Name, State
			Print_Debug = "All Application Pools: $allAppPool"
			$allAppPool = [Array]$allAppPool
			if ($allAppPool.Count -gt 0) {
				Print_Debug "Exclude list=$Exclude"
				if ($include) { 
					$totalAppPool = $include.Count
					foreach ($incl in $Include) {
						if (!$allAppPool.name.contains($incl)) {
							$failedAppPool += 1;
							$descErr += $incl
						}
					}
				}
				else { $totalAppPool = $allAppPool.Count }
				$reportPools = @()
				foreach ($pool in $allAppPool) {
					$skipPool = $False
					$AppPoolStatus = $pool.State
					$appName = $pool.Name
					$poolList += $appName + ", "
					Print_Debug "Debug: Name: $appName , Status: $AppPoolStatus"
					if ($Include -ne $null) {
						if (!$Include.tolower().Contains($appName.tolower())) {
							$skipPool = $True
							Print_Debug "Site $siteName is not in Include list: $Include, skipping $appName."
						}
					}
					if ($Exclude -ne $null) {
						foreach ($ex in $Exclude) {
							if ($appName -eq $ex) {
								$skipPool = $True
								Print_Debug "Application Pool $appName In Excluded list: $Exclude , skipping $appName ."
								break
							}
						}
					}	
					if ($AppPoolStatus -ne "Started" -and $skipPool -eq $False) {
						$failedAppPool += 1;
						$descErr += $appName 
					}
					else {
						if (!$skipPool) { $reportPools += $appName }
     }
				}
				$reportPools = $reportPools -join ', '
				$descErr = $descErr -join ', '
				if ($Exclude -ne $null) {
					$excludeCount = $Exclude.Count
					Print_Debug "Exclude Count: $excludeCount"
					$totalAppPool -= $excludeCount
					Print_Debug "Total App Pool after exclude: $totalAppPool"
				}
				$perfData = "|'Total failed Application Pools'=$failedAppPool"
				$Exclude = $Exclude -Join ', '
				if (!$reportPools) {$uptext=""} else {$uptext="; Up: ${reportPools}"}
				if (!$exclude) {$exclText=""} else {$exclText="; Exclude: ${Exclude}"}
			
				if ($failedAppPool -gt 0) {
					$desc = "$failedAppPool/$totalAppPool Application Pools are down; Down: $descErr${uptext}${exclText}: $perfData"
					$retCode = $critical	
				}
				else {
					Print_Debug "$totalAppPool/$totalAppPool  Application Pools are running. [$totalAppPool], Name: $poolList"
					$desc = "$totalAppPool/$totalAppPool Application Pools are running${upText}: $perfData"
					$retCode = $ok
				}
			}
			else {
				$desc = "No Web Application Pools found on $server"
				$retCode = $ok
			}
		}
		else {
			$desc = "An error occurred when trying to connect to the iis servers."
			$retCode = $unknown
		}
	}
	catch {
		$desc = $_.Exception.Message	
		Print_Debug $desc
	}
	return $retCode , $desc
}


Function Get_Sites_Status () {
	$retCode = $unknown
	$desc = $null
	$perfData = ""
	$failedSites = 0
	$descErr = @()
	$siteList = @()
	try {
		$serverManager = [Microsoft.Web.Administration.ServerManager]::OpenRemote($server)
		if ($serverManager -ne $null) {
			Print_Debug "Connected to $serverManager"
			$allSites = $serverManager.Sites | Select Name, ServerAutoStart, State
			$allSites = [Array]$allSites
			$reportSites = @()
			if ($allSites.Count -gt 0) {
				Print_Debug "Include list=$Include"
				Print_Debug "Exclude list=$Exclude"
				Print_Debug "All Sites: $allSites"
				if ($include) { 
					$totalSites = $include.Count
					foreach ($incl in $Include) {
						if (!$allsites.name.contains($incl)) {
							$failedSites += 1;
							$descErr += $incl
						}
					}
				}
				else { 
					$totalSites = $allSites.Count 
				}
				foreach ($site in $allSites) {
					$skipSite = $False
					$siteStatus = $site.State
					$siteName = $site.Name
					$siteList += $siteName + ", "
					Print_Debug "Debug: Name: $siteName , Status: $siteStatus"
					if ($Include) {
						if (!$Include.Contains($siteName)) {
							$skipSite = $True
							Print_Debug "Site $siteName is not in Include list: $Include, skipping $siteName."
						}
					}
					if ($Exclude) {
						foreach ($ex in $Exclude) {
							if ($siteName -eq $ex) {
								$skipSite = $True
								Print_Debug "Site $siteName In Excluded list: $Exclude , skipping $siteName ."
								break
							}
						}
					}
					if ($siteStatus -ne "Started" -and !$skipSite) {
						$failedSites += 1;
						$descErr += $siteName
					}
					else {
						if (!$skipSite) { 
							$reportSites += $siteName 
      }	
					}
				}
				if ($Exclude -ne $null) {
					$excludeCount = $Exclude.Count
					Print_Debug "Exclude Count: $excludeCount"
					$totalSites -= $excludeCount
					Print_Debug "Total Sites after exclude: $totalSites"
				}
				$descErr = $descErr -join ', '
				$reportSites = $reportSites -join ', '
				$perfData = "|'Total failed sites'=$failedSites"
				$Exclude = $Exclude -Join ', '
				if (!$reportSites) {$uptext=""} else {$uptext="; Up: ${reportSites}"}
				if (!$exclude) {$exclText=""} else {$exclText="; Exclude: ${Exclude}"}
 				if ($failedSites -gt 0) {
					$desc = "$failedSites/$totalSites website(s) are down; Down: $descErr${uptext}${exclText} $perfData"
					$retCode = $critical	
				}
				else {
					$siteList = $siteList -join ', '
					Print_Debug "All Web Sites ($reportSites) are running. [$totalSites], Name: $siteList"
					$desc = "$totalSites/$totalSites website(s) are running. Up: $reportSites${exclText} $perfData"
					$retCode = $ok
				}
			}
			else {
				$desc = "No Web Sites found on $server"
				$retCode = $ok
			}
		}
		else {
			$desc = "An error occurred when trying to connect to the iis servers."
			$retCode = $unknown
		}
	}
	catch {
		$desc = $_.Exception.Message	
		Print_Debug $desc
	}
	return $retCode , $desc
}

$ok = 0
$warning = 1
$critical = 2
$unknown = 3
$retCode = $unknown
$server = $env:COMPUTERNAME
if ($Include) {	
	$Include = $Include.Replace("""", "")
	$Include = $Include.Split(",") 

}
if ($Exclude) { 
	$Exclude = $Exclude.Replace("""", "")
	$Exclude = $Exclude.Split(",")
}

# Load IIS module
$loadIISModule , $desc = Load_IIS_Module						
if ($loadIISModule -eq $true) {
	Switch ($CheckType) {
		"Sites" {
			$check_status, $check_desc = Get_Sites_Status
			Print_Debug "Get_Sites_Status: $check_status msg: $check_desc" 
		}
		"AppPool" {
			$check_status, $check_desc = Get_AppPool_Status 
			Print_Debug "Get_AppPool_Status: $check_status msg: $check_desc" 
		}
	}
	$desc = $check_desc
	$retCode = $check_status
}

Print_Debug "retCode is: $retCode"

switch ($retCode) {
	0 { $msgPrefix = "OK" }
	1 { $msgPrefix = "WARNING" }
	2 { $msgPrefix = "CRITICAL" }
	3 { $msgPrefix = "UNKNOWN" }
		
}

write-host $msgPrefix":" $desc 
exit $retCode
